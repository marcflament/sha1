package org.yah.tools.sha1;

import static org.junit.Assert.assertArrayEquals;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractSHA1HasherTest {

	private Random random = new Random();

	private SHA1Hasher hasher;

	private MessageDigest javaDigest;

	@Before
	public void setup() throws NoSuchAlgorithmException {
		hasher = createCypher();
		javaDigest = MessageDigest.getInstance("SHA-1");
	}

	protected abstract SHA1Hasher createCypher();

	@Test
	public void sandbox() {
		byte[] hash = hash("abc");
		assertArrayEquals(hexToBytes("a9993e364706816aba3e25717850c26c9cd0d89d"), hash);

		hash = hash("abc");
		assertArrayEquals(hexToBytes("a9993e364706816aba3e25717850c26c9cd0d89d"), hash);
	}

	@Test
	public void hash_array() {
		assertArrayEquals(hexToBytes("2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"),
				hash("The quick brown fox jumps over the lazy dog"));

		assertArrayEquals(hexToBytes("de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3"),
				hash("The quick brown fox jumps over the lazy cog"));

		assertArrayEquals(hexToBytes("de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3"),
				hash("The quick brown fox jumps over the lazy cog"));

		assertArrayEquals(hexToBytes("d2f76f0b044c24c7941d42d2eddcb34ab54e1ca0"),
				hash("The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog"));

		assertArrayEquals(hexToBytes("da39a3ee5e6b4b0d3255bfef95601890afd80709"), hash(""));
	}

	@Test
	public void hash_stream() {
		assertArrayEquals(hexToBytes("2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"),
				hashStream("The quick brown fox jumps over the lazy dog"));

		assertArrayEquals(hexToBytes("de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3"),
				hashStream("The quick brown fox jumps over the lazy cog"));

		assertArrayEquals(hexToBytes("d2f76f0b044c24c7941d42d2eddcb34ab54e1ca0"),
				hashStream("The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog"));

		assertArrayEquals(hexToBytes("da39a3ee5e6b4b0d3255bfef95601890afd80709"), hashStream(""));
	}

	@Test
	public void compare_to_java() {
		for (int i = 0; i < 100; i++) {
			byte[] message = randomMessage(1024);
			assertArrayEquals(javaDigest.digest(message), hasher.hash(message));
		}
	}

	@Test
	public void compare_stream_to_java() {
		for (int i = 0; i < 100; i++) {
			byte[] message = randomMessage(1024);
			for (int j = 0; j < message.length; j++) {
				hasher.append(message,j,1);
			}
			assertArrayEquals(javaDigest.digest(message), hasher.hash());
		}
	}

	private byte[] randomMessage(int size) {
		byte[] res = new byte[size];
		random.nextBytes(res);
		return res;
	}

	private byte[] hash(String message) {
		byte[] bytes = message.getBytes(StandardCharsets.US_ASCII);
		return hasher.hash(bytes);
	}

	private byte[] hashStream(String message) {
		byte[] bytes = message.getBytes(StandardCharsets.US_ASCII);
		for (int i = 0; i < bytes.length; i++) {
			hasher.append(bytes, i, 1);
		}
		return hasher.hash();
	}

	private static byte[] hexToBytes(String string) {
		byte[] res = new byte[string.length() / 2];
		for (int i = 0; i < res.length; i++) {
			res[i] = (byte) Integer.parseInt(string.substring(i * 2, i * 2 + 2), 16);
		}
		return res;
	}

}
