package org.yah.tools.sha1.java.custom;

import org.yah.tools.sha1.AbstractSHA1HasherTest;
import org.yah.tools.sha1.SHA1Hasher;
import org.yah.tools.sha1.java.custom.ByteBufferSHA1Hasher;

public class ByteBufferSHA1HasherTest extends AbstractSHA1HasherTest {

	@Override
	protected SHA1Hasher createCypher() {
		return new ByteBufferSHA1Hasher();
	}

}
