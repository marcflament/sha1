package org.yah.tools.sha1;

public interface SHA1Hasher {

	void append(byte[] bytes, int offset, int length);

	byte[] hash(byte[] bytes, int offset, int length);

	void reset();

	default void append(byte[] bytes) {
		append(bytes, 0, bytes.length);
	}

	default byte[] hash(byte[] bytes) {
		return hash(bytes, 0, bytes.length);
	}

	default byte[] hash() {
		return hash(null, 0, 0);
	}

}
