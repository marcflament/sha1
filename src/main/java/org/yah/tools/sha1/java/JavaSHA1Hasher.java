package org.yah.tools.sha1.java;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.yah.tools.sha1.SHA1Hasher;

public class JavaSHA1Hasher implements SHA1Hasher {

	private final MessageDigest digest;
	
	public JavaSHA1Hasher() {
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void append(byte[] bytes, int offset, int length) {
		digest.update(bytes, offset, length);
	}

	@Override
	public byte[] hash(byte[] bytes, int offset, int length) {
		if (bytes != null) {
			digest.update(bytes, offset, length);
		}
		return digest.digest();
	}

	@Override
	public void reset() {
		digest.reset();
	}

}
