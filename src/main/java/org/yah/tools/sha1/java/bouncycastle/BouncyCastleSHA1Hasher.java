package org.yah.tools.sha1.java.bouncycastle;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.yah.tools.sha1.SHA1Hasher;

public class BouncyCastleSHA1Hasher implements SHA1Hasher {

	private final Digest digest;

	public BouncyCastleSHA1Hasher() {
		digest = new SHA1Digest();
	}

	@Override
	public void append(byte[] bytes, int offset, int length) {
		digest.update(bytes, offset, length);
	}

	@Override
	public byte[] hash(byte[] bytes, int offset, int length) {
		if (bytes != null) {
			digest.update(bytes, offset, length);
		}
		byte[] res = new byte[digest.getDigestSize()];
		digest.doFinal(res, 0);
		return res;
	}

	@Override
	public void reset() {
		digest.reset();
	}

}
