package org.yah.tools.sha1.java.custom;

import org.yah.tools.sha1.SHA1Hasher;


@SuppressWarnings("squid:S1659")
public final class ArraySHA1Hasher implements SHA1Hasher {

	private static final int[] H = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0 };

	// message length in bits (always a multiple of the number of bits in a
	// character).
	private long ml;

	// current chunk (512-bit = 64 bytes)
	private final byte[] chunk = new byte[64];

	private int chunkSize = 0;

	private final int[] words = new int[80];

	private int a, b, c, d, e;

	/**
	 * 160 bits (20 bytes = 5 ints) resulting hash
	 */
	private final int[] hash = new int[5];

	public ArraySHA1Hasher() {
		reset();
	}

	@Override
	public void reset() {
		System.arraycopy(H, 0, hash, 0, H.length);
		chunkSize = 0;
		ml = 0;
	}

	@Override
	public void append(byte[] bytes, int offset, int length) {
		ml += length << 3;
		if (ml < 0)
			throw new IllegalStateException("overflow: max message length is " + Long.MAX_VALUE);
		int remaining = length;
		while (remaining > 0) {
			int s = Math.min(chunk.length - chunkSize, remaining);
			System.arraycopy(bytes, offset, chunk, chunkSize, s);
			chunkSize += s;
			remaining -= s;
			offset += s;
			if (chunkSize == chunk.length) {
				update();
			}
		}
	}

	private void update() {
		createWords();

		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];

		int f, k;

		// Main loops
		for (int i = 0; i < 20; i++) {
			f = (b & c) | (~b & d);
			k = 0x5A827999;
			compute(f, k, words[i]);
		}

		for (int i = 20; i < 40; i++) {
			f = b ^ c ^ d;
			k = 0x6ED9EBA1;
			compute(f, k, words[i]);
		}

		for (int i = 40; i < 60; i++) {
			f = (b & c) | (b & d) | (c & d);
			k = 0x8F1BBCDC;
			compute(f, k, words[i]);
		}

		for (int i = 60; i < 80; i++) {
			f = b ^ c ^ d;
			k = 0xCA62C1D6;
			compute(f, k, words[i]);
		}

		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;

		chunkSize = 0;
	}

	// extend the sixteen 32-bit words into eighty 32-bit words:
	private void createWords() {
		for (int i = 0; i < 16; i++) {
			words[i] = bytesToInt(chunk, i << 2);
		}
		for (int i = 16; i < 80; i++) {
			int t = words[i - 3] ^ words[i - 8] ^ words[i - 14] ^ words[i - 16];
			words[i] = t << 1 | t >>> 31;
		}
	}

	@Override
	public byte[] hash(byte[] bytes, int offset, int length) {
		if (bytes != null) {
			append(bytes, offset, length);
		}
		completeChunk();
		byte[] res = produce();
		reset();
		return res;
	}

	/**
	 * Append the bit '1' to the message<br/>
	 * Append 0 ≤ k < 512 bits '0', such that the resulting message length in bits
	 * is congruent to −64 ≡ 448 (mod 512)<br/>
	 * Append ml, the original message length, as a 64-bit big-endian integer. Thus,
	 * the total length is a multiple of 512 bits.
	 */
	private void completeChunk() {
		// Append 10000000 to the message
		chunk[chunkSize++] = (byte) 0x80;
		if (chunkSize > 56) {
			// not space for message length, pad chunk with 0
			for (; chunkSize < chunk.length; chunkSize++) {
				chunk[chunkSize] = 0;
			}
			update();
		}

		// pad with 0 until there is only 8 bytes left (size of long)
		for (; chunkSize < 56; chunkSize++) {
			chunk[chunkSize] = 0;
		}

		// write the message size
		// could have used chunk.asLongBuffer(), but avoid any new allocation if
		// possible
		chunk[chunkSize++] = (byte) ((ml & 0xFF00000000000000l) >>> 56);
		chunk[chunkSize++] = (byte) ((ml & 0xFF000000000000l) >>> 48);
		chunk[chunkSize++] = (byte) ((ml & 0xFF0000000000l) >>> 40);
		chunk[chunkSize++] = (byte) ((ml & 0xFF00000000l) >>> 32);
		chunk[chunkSize++] = (byte) ((ml & 0xFF000000l) >>> 24);
		chunk[chunkSize++] = (byte) ((ml & 0xFF0000l) >>> 16);
		chunk[chunkSize++] = (byte) ((ml & 0xFF00l) >>> 8);
		chunk[chunkSize++] = (byte) ((ml & 0xFFl) & 0xFF);

		update();
	}

	private byte[] produce() {
		// Produce the final hash value (big-endian) as a 160-bit number:
		byte[] res = new byte[20];
		intToBytes(hash[0], res, 0);
		intToBytes(hash[1], res, 4);
		intToBytes(hash[2], res, 8);
		intToBytes(hash[3], res, 12);
		intToBytes(hash[4], res, 16);
		return res;
	}

	private void compute(int f, int k, int w) {
		int temp = (a << 5 | a >>> 27) + f + e + k + w;
		e = d;
		d = c;
		c = b << 30 | b >>> 2;
		b = a;
		a = temp;
	}

	private static int bytesToInt(byte[] bytes, int offset) {
		int res = (bytes[offset] & 0xFF) << 24;
		res |= (bytes[offset + 1] & 0xFF) << 16;
		res |= (bytes[offset + 2] & 0xFF) << 8;
		res |= (bytes[offset + 3] & 0xFF);
		return res;
	}

	private static void intToBytes(int i, byte[] res, int offset) {
		res[offset] = (byte) ((i & 0xFF000000) >>> 24);
		res[offset + 1] = (byte) ((i & 0xFF0000) >>> 16);
		res[offset + 2] = (byte) ((i & 0xFF00) >>> 8);
		res[offset + 3] = (byte) (i & 0xFF);
	}
}
