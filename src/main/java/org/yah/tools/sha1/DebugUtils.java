package org.yah.tools.sha1;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class DebugUtils {

	private DebugUtils() {}

	public static String toBinaryString(int w) {
		StringBuffer sb = new StringBuffer();
		ByteBuffer bb = byteBuffer(4);
		bb.asIntBuffer().put(w);
		for (int i = 0; i < bb.capacity(); i++) {
			int b = bb.get(i) & 0xFF;
			String s = Integer.toBinaryString(b);
			int padding = 8 - s.length();
			for (int j = 0; j < padding; j++) {
				sb.append('0');
			}
			sb.append(s).append(' ');
		}
		return sb.toString();
	}

	private static ByteBuffer byteBuffer(int size) {
		ByteBuffer bb = ByteBuffer.allocate(size);
		bb.order(ByteOrder.BIG_ENDIAN);
		return bb;
	}

}
