package org.yah.tools.sha1;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.infra.Blackhole;
import org.yah.tools.sha1.java.JavaSHA1Hasher;

public class JavaHasherBenchmark extends AbstractSha1Benchmark {

	@org.openjdk.jmh.annotations.State(Scope.Thread)
	public static class HasherState extends AbstractSha1State {

		@Override
		protected SHA1Hasher createHasher() {
			return new JavaSHA1Hasher();
		}

	}

	@Benchmark
	public void hash_array(HasherState hasherState, MessageState messageState, Blackhole blackhole) {
		hasherState.hash_array(messageState, blackhole);
	}

	@Benchmark
	public void hash_stream(HasherState hasherState, MessageState messageState, Blackhole blackhole) {
		hasherState.hash_stream(messageState, blackhole);
	}

}
