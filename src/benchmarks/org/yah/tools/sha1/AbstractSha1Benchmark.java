package org.yah.tools.sha1;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
public abstract class AbstractSha1Benchmark {

	@org.openjdk.jmh.annotations.State(Scope.Benchmark)
	public static class MessageState {

		private static final int DEFAULT_MESSAGE_SIZE = 12;

		protected byte[] message;

		@Setup(Level.Iteration)
		public void setup() {
			String property = System.getProperty("message.size");
			int messageSize;
			if (property == null)
				messageSize = DEFAULT_MESSAGE_SIZE;
			else
				messageSize = Integer.parseInt(property);
			message = new byte[messageSize];
			System.out.println("Message size: " + messageSize);
			new Random(215454864564l).nextBytes(message);
		}
	}

	protected static abstract class AbstractSha1State {

		private SHA1Hasher hasher;

		@Setup(Level.Iteration)
		public void setup() {
			hasher = createHasher();
		}

		protected abstract SHA1Hasher createHasher();

		public void hash_array(MessageState messageState, Blackhole blackhole) {
			blackhole.consume(hasher.hash(messageState.message));
		}

		public void hash_stream(MessageState messageState, Blackhole blackhole) {
			byte[] message = messageState.message;
			for (int i = 0; i < message.length; i++) {
				hasher.append(message, i, 1);
			}
			blackhole.consume(hasher.hash());
		}
	}

}
